package com.koidev.androidgpsbroadcastv2.business;

import android.location.Location;
import android.os.HandlerThread;
import android.text.TextUtils;

import com.koidev.androidgpsbroadcastv2.data.api.LoginApi;
import com.koidev.androidgpsbroadcastv2.data.database.realm.ContactsDataDBRealm;
import com.koidev.androidgpsbroadcastv2.data.database.realm.PersonalDataDBRealm;
import com.koidev.androidgpsbroadcastv2.data.network.model.response.RegistrationResponse;
import com.koidev.androidgpsbroadcastv2.data.network.model.response.StatusResponse;
import com.koidev.androidgpsbroadcastv2.data.repositories.IAppRepository;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

public class AppInteractor implements IAppInteractor {
    private final IAppRepository iAppRepository;
    private LoginApi api;

    public AppInteractor(IAppRepository iAppRepository, LoginApi api) {
        this.iAppRepository = iAppRepository;
        this.api = api;
    }

    @Override
    public Observable<RegistrationResponse> sendRequestLogin(String imei, String code) {
        return api.registrationRequest(imei, code);
    }

    @Override
    public Observable<StatusResponse> sendRequestLocationData(String imei, String code, String data,
                                                              String position, String speed, String battery) {
        return api.uploadData(imei, code, data, position, speed, battery);
    }

    @Override
    public void savePersonalDataCache(String imei, String code, String message, int central,
                                      int delay, boolean status) {
        iAppRepository.savePersonalDataCache(
                new PersonalDataDBRealm(imei, code, message, central, delay, status));
    }

    @Override
    public Observable<PersonalDataDBRealm> checkPersonalDataCache() {
        return iAppRepository.getPersonalDataCache();
    }

    @Override
    public void saveContactsCache(List<ContactsDataDBRealm> list) {
        iAppRepository.saveCheckedContacts(list);
    }

    @Override
    public Observable<ContactsDataDBRealm> checkContactsCache() {
        return iAppRepository.getCheckedList().flatMapIterable(list -> list);
    }

    @Override
    public Observable<ContactsDataDBRealm> getAllContacts() {
        return Observable.concat(iAppRepository.getCheckedList().flatMapIterable(list -> list),
                iAppRepository.getContactsList().flatMapIterable(list -> list))
                .distinct(ContactsDataDBRealm::getId);
    }

    @Override
    public Observable<String> getBatteryLevel() {
        return iAppRepository.getBatteryLevel();
    }

    @Override
    public Observable<Location> getLocation() {
        return iAppRepository.getCurrentLocation();
    }

    @Override
    public void closeRealm() {
        iAppRepository.closeRealm();
    }

    @Override
    public Observable<String> getMessageFromDB() {
        return iAppRepository.getPersonalDataCache()
                .map(personalData -> TextUtils.isEmpty(personalData.getMessage()) ? ""
                        : personalData.getMessage());
    }

    @Override
    public void saveMessageToDB(String message) {
        iAppRepository.saveMessage(message);
    }

    @Override
    public Scheduler getRealmScheduler() {
        return new Scheduler() {
            @Override
            public Worker createWorker() {
                HandlerThread thread = new HandlerThread("worker");
                if (!thread.isAlive()) thread.start();
                return AndroidSchedulers.from(thread.getLooper()).createWorker();
            }
        };
    }

    @Override
    public void startJobDispatcher() {
        iAppRepository.startJobDispatcher();
    }

    @Override
    public void stopJobDispatcher() {
        iAppRepository.stopJobDispatcher();
    }

    @Override
    public void setDelayToDB(int newDelay) {
        PersonalDataDBRealm dataDBRealm = new PersonalDataDBRealm();
        iAppRepository.getPersonalDataCache().subscribeOn(Schedulers.io()).subscribe(data -> {
            dataDBRealm.setImei(data.getImei());
            dataDBRealm.setCode(data.getCode());
            dataDBRealm.setMessage(data.getMessage());
            dataDBRealm.setCentral(data.getCentral());
            dataDBRealm.setDelay(newDelay);
        });
        iAppRepository.savePersonalDataCache(dataDBRealm);
    }

    @Override
    public Flowable<Realm> addListenerDataBase() {
        return iAppRepository.addListenerDataBase();
    }

    @Override
    public Single<Boolean> checkLocationSettingsGPS() {
        return iAppRepository.checkLocationSettingsGPS();
    }

    @Override
    public Observable<Boolean> isStatus() {
        return checkPersonalDataCache()
                .subscribeOn(Schedulers.io())
                .map(PersonalDataDBRealm::isStatus);
    }

}
