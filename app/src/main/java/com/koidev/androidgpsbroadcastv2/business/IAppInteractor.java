package com.koidev.androidgpsbroadcastv2.business;

import android.location.Location;

import com.koidev.androidgpsbroadcastv2.data.database.realm.ContactsDataDBRealm;
import com.koidev.androidgpsbroadcastv2.data.database.realm.PersonalDataDBRealm;
import com.koidev.androidgpsbroadcastv2.data.network.model.response.RegistrationResponse;
import com.koidev.androidgpsbroadcastv2.data.network.model.response.StatusResponse;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.realm.Realm;

public interface IAppInteractor {

    Observable<RegistrationResponse> sendRequestLogin(String imei, String code);

    Observable<StatusResponse> sendRequestLocationData(String imei, String code, String data,
                                                       String position, String speed, String battery);

    Observable<PersonalDataDBRealm> checkPersonalDataCache();

    void savePersonalDataCache(String imei, String code, String message, int central,
                               int delay, boolean status);

    void saveContactsCache(List<ContactsDataDBRealm> list);

    Observable<ContactsDataDBRealm> checkContactsCache();

    Observable<ContactsDataDBRealm> getAllContacts();

    Observable<String> getBatteryLevel();

    void closeRealm();

    Observable<Location> getLocation();

    Observable<String> getMessageFromDB();

    void saveMessageToDB(String message);

    Scheduler getRealmScheduler();

    void startJobDispatcher();

    void stopJobDispatcher();

    void setDelayToDB(int i);

    Flowable<Realm> addListenerDataBase();

    Single<Boolean> checkLocationSettingsGPS();

    Observable<Boolean> isStatus();
}
