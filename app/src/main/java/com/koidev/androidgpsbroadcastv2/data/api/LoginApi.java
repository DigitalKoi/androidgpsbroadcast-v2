package com.koidev.androidgpsbroadcastv2.data.api;

import com.koidev.androidgpsbroadcastv2.data.network.model.response.RegistrationResponse;
import com.koidev.androidgpsbroadcastv2.data.network.model.response.StatusResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LoginApi {

    @GET("gpsalarmcontrol.php?a=Reg")
    Observable<RegistrationResponse> registrationRequest(
            @Query("i") String imei, @Query("c") String code);

    @GET("gpsalarmcontrol.php?a=Upl")
    Observable<StatusResponse> uploadData(
            @Query("i") String imei, @Query("c") String code, @Query("d") String datatime,
            @Query("p") String location, @Query("s") String speed, @Query("b") String battery);
}