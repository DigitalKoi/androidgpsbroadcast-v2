package com.koidev.androidgpsbroadcastv2.data.database.realm;

import io.realm.RealmObject;

public class PersonalDataDBRealm extends RealmObject {

    private String imei;
    private String code;
    private String message;
    private int central;
    private int delay;
    private boolean status;

    public PersonalDataDBRealm() {
    }

    public PersonalDataDBRealm(String imei, String code, String message, int central, int delay, boolean status) {
        this.imei = imei;
        this.code = code;
        this.message = message;
        this.central = central;
        this.delay = delay;
        this.status = status;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCentral() {
        return central;
    }

    public void setCentral(int central) {
        this.central = central;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
