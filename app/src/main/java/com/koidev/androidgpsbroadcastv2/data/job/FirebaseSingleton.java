package com.koidev.androidgpsbroadcastv2.data.job;


import android.content.Context;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;

public class FirebaseSingleton {
    private static FirebaseJobDispatcher firebaseJobDispatcher;

    private FirebaseSingleton() {}

    public static FirebaseJobDispatcher getInstance(Context context) {
        if (firebaseJobDispatcher == null) {
            firebaseJobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
        }
        return firebaseJobDispatcher;
    }
}
