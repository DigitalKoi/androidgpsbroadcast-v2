package com.koidev.androidgpsbroadcastv2.data.network.model.request;

import java.io.Serializable;

public class StatusViewModel {

    public enum State implements Serializable {
        LOADED, LOADING, ERROR, EMPTY
    }

    public enum ErrorType implements Serializable {
        NETWORK
    }

    private String status;
    private String message;
    private String central;
    private String delay;
    private ErrorType errorType;
    private State state = State.EMPTY;

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public StatusViewModel(String status, String message, String central, String delay, State state) {
        this.status = status;
        this.message = message;
        this.central = central;
        this.delay = delay;
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCentral() {
        return central;
    }

    public void setCentral(String central) {
        this.central = central;
    }

    public String getDelay() {
        return delay;
    }

    public void setDelay(String delay) {
        this.delay = delay;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "StatusViewModel{"
                + "status='"
                + status
                + '\''
                + ", message='"
                + message
                + '\''
                + ", central='"
                + central
                + '\''
                + ", delay='"
                + delay
                + '\''
                + ", errorType="
                + errorType
                + ", state="
                + state
                + '}';
    }
}
