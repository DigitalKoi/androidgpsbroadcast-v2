package com.koidev.androidgpsbroadcastv2.data.network.model.request;


import android.location.Location;

import com.koidev.androidgpsbroadcastv2.data.database.realm.PersonalDataDBRealm;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataCache {
   public final String imei;
   public final String code;
   public final String time;
   public final String location;
   public final String speed;
   public final String battery;
   public final String central;
   public final int delay;

    public DataCache(Location location, PersonalDataDBRealm data, String battery) {
        this.imei = data.getImei();
        this.code = data.getCode();
        this.central = String.valueOf(data.getCentral());
        this.time = convertDate(location.getTime());
        this.delay = data.getDelay();
        this.location = location.getLatitude() + "|" + location.getLongitude();
        this.speed = String.valueOf(location.getSpeed());
        this.battery = battery;
    }

    @Override
    public String toString() {
        return "i='" + imei + '\'' +
                ", c='" + code + '\'' +
                ", t='" + time + '\'' +
                ", l='" + location + '\'' +
                ", s='" + speed + '\'' +
                ", b='" + battery;
    }

    private String convertDate(Long longDate) {
        Date date = new Date(longDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(date) + "T" + timeFormat.format(date);
    }
}
