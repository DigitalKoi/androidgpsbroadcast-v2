package com.koidev.androidgpsbroadcastv2.data.utils;

import android.os.HandlerThread;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class RealmScheduler {

    public RealmScheduler() {
    }

    public Scheduler provideRealmScheduler() {
        return new Scheduler() {
            @Override
            public Scheduler.Worker createWorker() {
                HandlerThread thread = new HandlerThread("worker");
                /*if (!thread.isAlive())*/
                thread.start();
                return AndroidSchedulers.from(thread.getLooper()).createWorker();
            }
        };
    }
}
