package com.koidev.androidgpsbroadcastv2.data.repositories;

import android.location.Location;

import com.koidev.androidgpsbroadcastv2.data.database.realm.ContactsDataDBRealm;
import com.koidev.androidgpsbroadcastv2.data.database.realm.PersonalDataDBRealm;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.realm.Realm;

public interface IAppRepository {
    Observable<Location> getCurrentLocation();

    void savePersonalDataCache(PersonalDataDBRealm data);

    Observable<List<ContactsDataDBRealm>> getContactsList();

    Observable<List<ContactsDataDBRealm>> getCheckedList();

    void saveCheckedContacts(List<ContactsDataDBRealm> list);

    Observable<PersonalDataDBRealm> getPersonalDataCache();

    void closeRealm();

    Observable<String> getBatteryLevel();

    void startJobDispatcher();

    void stopJobDispatcher();

    Flowable<Realm> addListenerDataBase();

    Single<Boolean> checkLocationSettingsGPS();

    void saveMessage(String message);

}
