package com.koidev.androidgpsbroadcastv2.data.location;

import android.location.Location;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface LocationProvider {
    Observable<Location> getCurrentLocation();

    Single<Boolean> checkLocationSettingsGPS();
}
