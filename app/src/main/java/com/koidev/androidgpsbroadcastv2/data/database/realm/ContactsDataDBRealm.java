package com.koidev.androidgpsbroadcastv2.data.database.realm;

import io.realm.RealmObject;

public class ContactsDataDBRealm extends RealmObject {

    private long id;
    private String name;
    private String number;
    private String imageUri;
    private boolean checked;

    public ContactsDataDBRealm() {
    }

    public ContactsDataDBRealm(long id, String name, String number, String imageUri, boolean checked) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.imageUri = imageUri;
        this.checked = checked;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
