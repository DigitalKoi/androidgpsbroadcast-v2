package com.koidev.androidgpsbroadcastv2.data.job;

import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;

/**
 * @author Taras Zhupnyk (akka DigitalKoi) on 14/02/18.
 */

class JobRunableSinglton {
    private static JobRunnable jobRunnable;

    static JobRunnable getInstance(IAppInteractor interactor) {
        if (jobRunnable == null) {
            jobRunnable = new JobRunnable(interactor);
        }
        return jobRunnable;
    }
}
