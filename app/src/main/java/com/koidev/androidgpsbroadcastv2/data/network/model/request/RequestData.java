package com.koidev.androidgpsbroadcastv2.data.network.model.request;

import android.location.Location;

import com.koidev.androidgpsbroadcastv2.data.database.realm.PersonalDataDBRealm;

public class RequestData {
    final public Location location;
    final public PersonalDataDBRealm personalDataDBRealm;
    final public String batteryLevel;

    public RequestData(Location location, PersonalDataDBRealm personalDataDBRealm,
                       String batteryLevel) {
        this.location = location;
        this.personalDataDBRealm = personalDataDBRealm;
        this.batteryLevel = batteryLevel;
    }
}
