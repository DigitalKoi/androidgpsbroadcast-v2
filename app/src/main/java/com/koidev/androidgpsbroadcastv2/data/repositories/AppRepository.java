package com.koidev.androidgpsbroadcastv2.data.repositories;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.provider.ContactsContract;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;
import com.koidev.androidgpsbroadcastv2.data.database.realm.ContactsDataDBRealm;
import com.koidev.androidgpsbroadcastv2.data.database.realm.PersonalDataDBRealm;
import com.koidev.androidgpsbroadcastv2.data.database.realm.RealmHelper;
import com.koidev.androidgpsbroadcastv2.data.location.LocationProvider;
import com.koidev.androidgpsbroadcastv2.data.job.JobRequestService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

public class AppRepository implements IAppRepository {

    private LocationProvider locationProvider;
    private RealmHelper realmHelper;
    private Context context;
    private FirebaseJobDispatcher dispatcher;

    public AppRepository(Context context, RealmHelper realmHelper,
                         LocationProvider locationProvider) {
        this.context = context;
        this.realmHelper = realmHelper;
        this.locationProvider = locationProvider;
    }

    @Override
    public Observable<Location> getCurrentLocation() {
        return locationProvider.getCurrentLocation();
    }

    @Override
    public void savePersonalDataCache(PersonalDataDBRealm data) {
        realmHelper.savePersonalData(data);
    }

    @Override
    public void saveCheckedContacts(List<ContactsDataDBRealm> list) {
        realmHelper.saveCheckedContactsToRealm(list);
    }

    @Override
    public Observable<PersonalDataDBRealm> getPersonalDataCache() {
        return realmHelper.checkIfCachedPersonalDB();
    }

    @Override
    public void closeRealm() {
        realmHelper.closeRealm();
    }

    @Override
    public Observable<String> getBatteryLevel() {
        return Observable.fromCallable(this::getBatteryLevelIntent);
    }

    @Override
    public void startJobDispatcher() {
        saveStatus(true);
        dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
        Job jobRequest = dispatcher.newJobBuilder()
                .setService(JobRequestService.class)
                .setTag("job_service")
                .setRecurring(true)
                .setReplaceCurrent(true)
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                .setTrigger(Trigger.executionWindow(0, 1))
                .build();
        dispatcher.mustSchedule(jobRequest);
    }

    @Override
    public void stopJobDispatcher() {
        saveStatus(false);

        dispatcher.cancel("job_service");
        dispatcher.cancelAll();
        closeRealm();
    }

    @Override
    public Flowable<Realm> addListenerDataBase() {
        return realmHelper.listenerDataBase();
    }

    @Override
    public Single<Boolean> checkLocationSettingsGPS() {
        return locationProvider.checkLocationSettingsGPS();
    }

    @Override
    public void saveMessage(String message) {
        getPersonalDataCache()
                .subscribeOn(Schedulers.io())
                .map(data -> new PersonalDataDBRealm(
                        data.getImei(),
                        data.getCode(),
                        message,
                        data.getCentral(),
                        data.getDelay(),
                        data.isStatus()))
                .subscribe(this::savePersonalDataCache);
    }

    private void saveStatus(boolean status) {
        getPersonalDataCache()
                .subscribeOn(Schedulers.io())
                .map(data -> new PersonalDataDBRealm(
                        data.getImei(),
                        data.getCode(),
                        data.getMessage(),
                        data.getCentral(),
                        data.getDelay(),
                        status))
                .subscribe(this::savePersonalDataCache);
    }


    @Override
    public Observable<List<ContactsDataDBRealm>> getCheckedList() {
        return realmHelper.checkIfCachedContactsDB();
    }

    @Override
    public Observable<List<ContactsDataDBRealm>> getContactsList() {
        return Observable.fromCallable(this::getContactsListFromBook);
    }

    private String getBatteryLevelIntent() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent battery = context.registerReceiver(null, intentFilter);
        int level = battery != null ? battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : 0;
        return String.valueOf(level);
    }

    //ContactList from phone book
    private List<ContactsDataDBRealm> getContactsListFromBook() {
        List<ContactsDataDBRealm> list = new ArrayList<>();
        ContentResolver resolver = context.getContentResolver();
        Cursor cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                int hasPhoneNumber = Integer.parseInt(
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));

                if (hasPhoneNumber > 0) {
                    Cursor cursor1 = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id},
                            null);

                    while (cursor1.moveToNext()) {
                        String phoneNumber = cursor1.getString(
                                cursor1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        Uri idUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI,
                                Long.parseLong(id));
                        Uri imageUri =
                                Uri.withAppendedPath(idUri, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);
                        boolean checked = false;
                        list.add(
                                new ContactsDataDBRealm(Long.parseLong(id), name, phoneNumber, imageUri.toString(),
                                        checked));
                    }
                    cursor1.close();
                }
            }
        }
        cursor.close();

        return list;
    }
}
