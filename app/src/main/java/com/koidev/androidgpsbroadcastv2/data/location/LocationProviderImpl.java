package com.koidev.androidgpsbroadcastv2.data.location;

import com.google.android.gms.location.LocationRequest;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.patloew.rxlocation.RxLocation;

import io.reactivex.Observable;
import io.reactivex.Single;

public class LocationProviderImpl implements LocationProvider {

    private RxLocation rxLocation;
    private Context context;
    private LocationRequest locationRequest;

    public LocationProviderImpl(Context context) {
        this.context = context;
        rxLocation = new RxLocation(context);
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1000);
    }

    @Override
    public Observable<Location> getCurrentLocation() {

        return rxLocation.settings().checkAndHandleResolution(locationRequest)
                .flatMapObservable(this::getAddressObservable);
    }

    @Override
    public Single<Boolean> checkLocationSettingsGPS() {
        return rxLocation.settings().checkAndHandleResolution(locationRequest);
    }

    private Observable<Location> getAddressObservable(boolean success) {
        if (success) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return Observable.fromCallable(() -> {
                    throw new PermisionException();
                });
            }
        }
        Log.i("LoProImpl", "getAddressObservable");
        return rxLocation.location().updates(locationRequest);
    }

}
