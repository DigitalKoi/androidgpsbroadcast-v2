package com.koidev.androidgpsbroadcastv2.data.network.model.request;

import com.koidev.androidgpsbroadcastv2.data.api.LoginApi;

import com.koidev.androidgpsbroadcastv2.data.network.model.response.RegistrationResponse;
import com.koidev.androidgpsbroadcastv2.data.network.model.response.StatusResponse;

import io.reactivex.Observable;

public class ApiWrapper {

    private LoginApi api;

    public ApiWrapper(LoginApi api) {
        this.api = api;
    }

    public Observable<RegistrationResponse> requestLogin(String imei, String code) {
        return api.registrationRequest(imei, code);
    }

    public Observable<StatusResponse> requestLocationData(String imei, String code,
                                                          String data, String position, String speed, String battery) {
        return api.uploadData(imei, code, data, position, speed, battery);
    }
}
