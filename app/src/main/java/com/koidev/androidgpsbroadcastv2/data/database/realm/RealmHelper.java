package com.koidev.androidgpsbroadcastv2.data.database.realm;

import android.os.HandlerThread;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposables;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class RealmHelper {

    public RealmHelper() {
    }

    //ContactsDB
    public Observable<List<ContactsDataDBRealm>> checkIfCachedContactsDB() {
        return Observable.create(emitter -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<ContactsDataDBRealm> realmResults = realm.where(ContactsDataDBRealm.class).findAll();
            emitter.onNext(realm.copyFromRealm(realmResults));
        });

    }

    public void saveCheckedContactsToRealm(List<ContactsDataDBRealm> list) {
        Observable.create(e -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.delete(ContactsDataDBRealm.class);
            realm.copyToRealm(list);
            realm.commitTransaction();
        })
                .subscribeOn(getRealmReadScheduler())
                .unsubscribeOn(getRealmReadScheduler())
                .subscribe();
    }

    //PersonalDB
    public void savePersonalData(PersonalDataDBRealm personalDataDBRealm) {
        Observable.create(e -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.delete(PersonalDataDBRealm.class);
            List<PersonalDataDBRealm> list = new ArrayList<>();
            list.add(personalDataDBRealm);
            realm.copyToRealm(list);
            realm.commitTransaction();
            realm.close();
            Log.i("RealmHelper", "savePersonalData" + String.valueOf(personalDataDBRealm.isStatus()));
        })
                .subscribeOn(getRealmReadScheduler())
                .observeOn(getRealmReadScheduler())
                .subscribe();
    }

    public Observable<PersonalDataDBRealm> checkIfCachedPersonalDB() {
        return Observable.create(e -> {
            Realm realm = Realm.getDefaultInstance();
            PersonalDataDBRealm first = realm.where(PersonalDataDBRealm.class).findFirst();
            assert first != null;
            Log.i("RealmHelper", "checkIfCachedPersonalDB: " + String.valueOf(first.isStatus()));
            if (first != null) {
                e.onNext(first);
                realm.close();
            } else {
                e.onNext(new PersonalDataDBRealm());
            }
        });
    }

    public void closeRealm() {
        Observable.create(e -> {
            Realm realm = Realm.getDefaultInstance();
            Log.i("RealmHelper", "closeRealm: " + Thread.currentThread().getName());
            realm.close();
        })
                .subscribeOn(getRealmReadScheduler())
                .observeOn(getRealmReadScheduler())
                .subscribe();
    }

    public Flowable<Realm> listenerDataBase() {
        return Flowable.create(emitter -> {
            final Realm observableRealm = Realm.getDefaultInstance();
            final RealmChangeListener<Realm> listener = realm -> {
                if (!emitter.isCancelled()) {
                    emitter.onNext(realm);
                }
            };
            observableRealm.addChangeListener(listener);
            // Cleanup when stream is disposed
            emitter.setDisposable(Disposables.fromRunnable(() -> {
                observableRealm.removeChangeListener(listener);
                observableRealm.close();
            }));
            // Emit current value immediately
            emitter.onNext(observableRealm);
        }, BackpressureStrategy.LATEST);
    }

    private Scheduler getRealmReadScheduler() {
        return new Scheduler() {
            @Override
            public Scheduler.Worker createWorker() {
                HandlerThread thread = new HandlerThread("worker");
                if (!thread.isAlive()) thread.start();
                return AndroidSchedulers.from(thread.getLooper()).createWorker();
            }
        };
    }
}
