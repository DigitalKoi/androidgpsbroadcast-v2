package com.koidev.androidgpsbroadcastv2.data.job;

import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.data.database.realm.PersonalDataDBRealm;
import com.koidev.androidgpsbroadcastv2.data.network.model.response.StatusResponse;
import com.koidev.androidgpsbroadcastv2.data.network.model.request.DataCache;


import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class JobRunnable implements Runnable {

    private static final String TAG = JobRunnable.class.getSimpleName();
    private static IAppInteractor interactor;

    private CompositeDisposable disposable = new CompositeDisposable();

    private Handler handler = new Handler(Looper.myLooper());
    private DataCache dataCache;

    JobRunnable(IAppInteractor interactor) {
        JobRunnable.interactor = interactor;
        startJob();
    }

    @Override
    public void run() {
        Observable.zip(interactor.checkPersonalDataCache(),
                interactor.getLocation(),
                interactor.getBatteryLevel(),
                this::cacheData)
                .doOnSubscribe(disposable::add)
                .filter(data -> data.delay != 0)
                .flatMap(this::sendData)
                .doOnError(this::convertError)
                .map((StatusResponse statusResponse) -> {
                    try {
                        Log.i(TAG, "onNext");
                        long delay = (long) statusResponse.getDelay() * 1000;

                        if (delay != 0 && statusResponse.getStatus() != 0) {
                            handler.postDelayed(JobRunnable.this, delay);
                            disposable.clear();
                        } else {
                            closeHandler();
                        }
                        return statusResponse;
                    } catch (Exception e) {
                        return Observable.error(e);
                    }
                })
                .onErrorReturn(Observable::error)
                .subscribe();
    }

    private void startJob() {
        interactor.isStatus().subscribe(startJob -> {
            if (startJob) {
                handler.post(this);
            } else closeHandler();
        });
    }

    void closeHandler() {
        Log.i(TAG, "closeHandler");
        interactor.setDelayToDB(0);
        handler.removeCallbacks(this);
        disposable.clear();
        interactor.stopJobDispatcher();
    }

    private DataCache cacheData(PersonalDataDBRealm data, Location location, String battery) {
        return dataCache = new DataCache(location, data, battery);
    }

    private Observable<StatusResponse> sendData(DataCache dataCache) {
        return interactor.sendRequestLocationData(
                dataCache.imei, dataCache.code,
                dataCache.time,
                dataCache.location,
                dataCache.speed, dataCache.battery
        ).subscribeOn(Schedulers.io());
    }

    private void convertError(Throwable throwable) {
        sendMessageSMS();
    }

    private void sendMessageSMS() {
        Observable.empty()
                .doOnComplete(() -> {
                    //Sending SMS
                    try {
                        SmsManager smsManager = SmsManager.getDefault();
                        String centralNumber = dataCache.central;
                        String message = dataCache.toString();
                        smsManager.sendTextMessage(
                                centralNumber,
                                null,
                                message,
                                null,
                                null);

                        closeHandler();
                    } catch (Exception e) {

                    }
                }).subscribe();
    }

}
