package com.koidev.androidgpsbroadcastv2.data.job;

import android.os.PowerManager;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.koidev.androidgpsbroadcastv2.BroadcastApplication;
import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.di.job.JobModule;

import javax.inject.Inject;


public class JobRequestService extends JobService {

    public static final String TAG = JobRequestService.class.getSimpleName();

    private JobRunnable jobRunnableRunnable;

    @Inject
    IAppInteractor interactor;

    private PowerManager.WakeLock wakeLock;

    @Override
    public void onCreate() {
        super.onCreate();
        BroadcastApplication.get(getApplicationContext())
                .applicationComponent()
                .plus(new JobModule())
                .inject(this);
    }

    @Override
    public boolean onStartJob(JobParameters job) {
        Log.i(TAG, "onStartJob: ");
        this.configurePowerManager();
        jobRunnableRunnable = JobRunableSinglton.getInstance(interactor);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        Log.i(TAG, "onStopJob");
        jobRunnableRunnable.closeHandler();
        return false;
    }

    private void configurePowerManager() {
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        if (powerManager != null) {
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "jobService");
            wakeLock.acquire();
        }
    }

}
