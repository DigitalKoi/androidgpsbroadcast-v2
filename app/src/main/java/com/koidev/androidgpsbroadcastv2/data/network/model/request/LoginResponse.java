package com.koidev.androidgpsbroadcastv2.data.network.model.request;

public class LoginResponse {

    private int status;
    private String message;
    private String centralNumber;
    private String delay;

    public LoginResponse(int status, String message, String centralNumber, String delay) {
        this.status = status;
        this.message = message;
        this.centralNumber = centralNumber;
        this.delay = delay;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getCentralNumber() {
        return centralNumber;
    }

    public String getDelay() {
        return delay;
    }
}
