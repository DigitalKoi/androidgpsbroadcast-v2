package com.koidev.androidgpsbroadcastv2;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.koidev.androidgpsbroadcastv2.di.application.AppComponent;
import com.koidev.androidgpsbroadcastv2.di.application.AppModule;
import com.koidev.androidgpsbroadcastv2.di.application.DaggerAppComponent;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BroadcastApplication extends Application {
    @SuppressWarnings("NullableProblems")
    @NonNull
    private AppComponent appComponent;

    @NonNull
    public static BroadcastApplication get(@NonNull Context context) {
        return (BroadcastApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        prepareAppComponent();
        initRealmConfiguration();
    }

    private void prepareAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    private void initRealmConfiguration() {
        Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().schemaVersion(1)
                //.migration(new RealmMigration())
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    @NonNull
    public AppComponent applicationComponent() {
        return appComponent;
    }
}
