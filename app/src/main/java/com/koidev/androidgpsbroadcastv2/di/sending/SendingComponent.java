package com.koidev.androidgpsbroadcastv2.di.sending;

import com.koidev.androidgpsbroadcastv2.ui.sending.SendingFragment;

import dagger.Subcomponent;

@Subcomponent(modules = SendingModule.class)
@SendingScope
public interface SendingComponent {
    void inject(SendingFragment fragment);
}
