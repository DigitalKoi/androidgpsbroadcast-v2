package com.koidev.androidgpsbroadcastv2.di.settings;

import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.ui.settings.ISettings;
import com.koidev.androidgpsbroadcastv2.ui.settings.SettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingsModule {

    @Provides
    @SettingsScope
    ISettings.Presenter provideSettingsPresenter(
            IAppInteractor interactor) {
        return new SettingsPresenter(interactor);
    }
}
