package com.koidev.androidgpsbroadcastv2.di.splash;

import com.koidev.androidgpsbroadcastv2.ui.splash.SplashFragment;

import dagger.Subcomponent;

@Subcomponent(modules = SplashModule.class)
@SplashScope
public interface SplashComponent {
    void inject(SplashFragment fragment);
}
