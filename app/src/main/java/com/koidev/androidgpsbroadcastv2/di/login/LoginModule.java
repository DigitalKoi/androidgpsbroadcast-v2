package com.koidev.androidgpsbroadcastv2.di.login;

import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.ui.login.ILogin;
import com.koidev.androidgpsbroadcastv2.ui.login.LoginPresenter;
import com.koidev.androidgpsbroadcastv2.data.utils.RealmScheduler;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

    @Provides
    @LoginScope
    ILogin.Presenter provideILoginPresenter(IAppInteractor interactor, RealmScheduler scheduler) {
        return new LoginPresenter(interactor, scheduler);
    }

}
