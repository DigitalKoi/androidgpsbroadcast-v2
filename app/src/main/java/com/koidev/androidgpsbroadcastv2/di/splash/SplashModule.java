package com.koidev.androidgpsbroadcastv2.di.splash;

import com.koidev.androidgpsbroadcastv2.ui.splash.ISplash;
import com.koidev.androidgpsbroadcastv2.ui.splash.SplashPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashModule {

    @Provides
    @SplashScope
    ISplash.Presenter provideISplashPresenter() {
        return new SplashPresenter();
    }
}
