package com.koidev.androidgpsbroadcastv2.di.sending;

import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.ui.sending.ISending;
import com.koidev.androidgpsbroadcastv2.ui.sending.SendingPresenter;
import com.koidev.androidgpsbroadcastv2.data.utils.RealmScheduler;

import dagger.Module;
import dagger.Provides;

@Module
public class SendingModule {

    @Provides
    @SendingScope
    ISending.Presenter provideISendingPresenter(IAppInteractor interactor) {
        return new SendingPresenter(interactor);
    }
}
