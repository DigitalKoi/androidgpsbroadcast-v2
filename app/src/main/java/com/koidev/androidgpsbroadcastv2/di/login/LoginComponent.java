package com.koidev.androidgpsbroadcastv2.di.login;

import com.koidev.androidgpsbroadcastv2.ui.login.LoginFragment;

import dagger.Subcomponent;

@Subcomponent(modules = LoginModule.class)
@LoginScope
public interface LoginComponent {
    void inject(LoginFragment fragment);
}
