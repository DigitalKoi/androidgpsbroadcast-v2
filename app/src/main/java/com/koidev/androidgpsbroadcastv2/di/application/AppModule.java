package com.koidev.androidgpsbroadcastv2.di.application;

import android.content.Context;
import android.support.annotation.NonNull;

import com.koidev.androidgpsbroadcastv2.data.api.LoginApi;
import com.koidev.androidgpsbroadcastv2.business.AppInteractor;
import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.data.database.realm.RealmHelper;
import com.koidev.androidgpsbroadcastv2.data.location.LocationProvider;
import com.koidev.androidgpsbroadcastv2.data.location.LocationProviderImpl;
import com.koidev.androidgpsbroadcastv2.data.network.model.request.ApiWrapper;
import com.koidev.androidgpsbroadcastv2.data.repositories.AppRepository;
import com.koidev.androidgpsbroadcastv2.data.repositories.IAppRepository;
import com.koidev.androidgpsbroadcastv2.data.utils.Constants;
import com.koidev.androidgpsbroadcastv2.data.utils.RealmScheduler;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {
    private final Context appContext;

    public AppModule(@NonNull Context context) {
        appContext = context;
    }

    @Provides
    @PerApplication
    Context provideContext() {
        return appContext;
    }

    //
//  @Provides @PerApplication Observable<Realm> provideRealm() {
//    return Observable.fromCallable(Realm::getDefaultInstance);
//  }
//
    @Provides
    @PerApplication
    RealmHelper provideRealmHelper() {
        return new RealmHelper();
    }

    @Provides
    @PerApplication
    RealmScheduler provideRealmScheduler() {
        return new RealmScheduler();
    }

    @Provides
    @PerApplication
    IAppRepository provideIAppRepository(Context appContext,
                                         RealmHelper realmHelper,
                                         LocationProvider locationProvider) {
        return new AppRepository(appContext, realmHelper, locationProvider);
    }

    @Provides
    @PerApplication
    IAppInteractor provideAppInteractor(IAppRepository repository,
                                        LoginApi api) {
        return new AppInteractor(repository, api);
    }

    @Provides
    @PerApplication
    OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return
                new OkHttpClient.Builder()
                        .addInterceptor(httpLoggingInterceptor)
                        .build();
    }

    @Provides
    @PerApplication
    RxJava2CallAdapterFactory provideRxJava2CallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @PerApplication
    GsonConverterFactory provideScalarsConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @PerApplication
    Retrofit provideRetrofit(GsonConverterFactory factory,
                             RxJava2CallAdapterFactory adapter, OkHttpClient client) {
        return new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addCallAdapterFactory(adapter)
                .addConverterFactory(factory)
                .client(client)
                .build();
    }

    @Provides
    @PerApplication
    LoginApi provideApi(Retrofit retrofit) {
        return retrofit.create(LoginApi.class);
    }

    @Provides
    @PerApplication
    ApiWrapper provideApiWrapper(LoginApi api) {
        return new ApiWrapper(api);
    }

    @Provides
    @PerApplication
    LocationProvider provideLocationProvider(Context context) {
        return new LocationProviderImpl(context);
    }

}
