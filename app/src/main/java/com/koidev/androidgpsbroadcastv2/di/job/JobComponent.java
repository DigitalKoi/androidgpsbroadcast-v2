package com.koidev.androidgpsbroadcastv2.di.job;

import com.koidev.androidgpsbroadcastv2.data.job.JobRequestService;

import dagger.Subcomponent;

@Subcomponent(modules = JobModule.class)
@JobScope
public interface JobComponent {
    void inject(JobRequestService service);
}
