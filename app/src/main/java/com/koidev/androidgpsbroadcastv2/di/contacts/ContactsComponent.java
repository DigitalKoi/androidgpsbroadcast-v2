package com.koidev.androidgpsbroadcastv2.di.contacts;

import com.koidev.androidgpsbroadcastv2.ui.contacts.ContactsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = ContactsModule.class)
@ContactsScope
public interface ContactsComponent {
    void inject(ContactsFragment fragment);
}
