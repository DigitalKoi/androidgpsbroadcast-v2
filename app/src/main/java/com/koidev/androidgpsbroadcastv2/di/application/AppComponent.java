package com.koidev.androidgpsbroadcastv2.di.application;

import com.koidev.androidgpsbroadcastv2.di.contacts.ContactsComponent;
import com.koidev.androidgpsbroadcastv2.di.contacts.ContactsModule;
import com.koidev.androidgpsbroadcastv2.di.job.JobComponent;
import com.koidev.androidgpsbroadcastv2.di.job.JobModule;
import com.koidev.androidgpsbroadcastv2.di.login.LoginComponent;
import com.koidev.androidgpsbroadcastv2.di.login.LoginModule;
import com.koidev.androidgpsbroadcastv2.di.sending.SendingComponent;
import com.koidev.androidgpsbroadcastv2.di.sending.SendingModule;
import com.koidev.androidgpsbroadcastv2.di.settings.SettingsComponent;
import com.koidev.androidgpsbroadcastv2.di.settings.SettingsModule;
import com.koidev.androidgpsbroadcastv2.di.splash.SplashComponent;
import com.koidev.androidgpsbroadcastv2.di.splash.SplashModule;

import dagger.Component;
import retrofit2.Retrofit;

@Component(modules = AppModule.class)
@PerApplication
public interface AppComponent {

    LoginComponent plus(LoginModule module);

    SplashComponent plus(SplashModule module);

    SettingsComponent plus(SettingsModule module);

    ContactsComponent plus(ContactsModule module);

    SendingComponent plus(SendingModule module);

    JobComponent plus(JobModule module);

//  Observable<Realm> realm();

    Retrofit retrofit();
}
