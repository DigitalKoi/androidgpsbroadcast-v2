package com.koidev.androidgpsbroadcastv2.di.contacts;

import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.ui.contacts.ContactsPresenter;
import com.koidev.androidgpsbroadcastv2.ui.contacts.IContacts;

import dagger.Module;
import dagger.Provides;

@Module
public class ContactsModule {

    @Provides
    @ContactsScope
    IContacts.Presenter provideIContactsPresenter(IAppInteractor interactor) {
        return new ContactsPresenter(interactor);
    }
}
