package com.koidev.androidgpsbroadcastv2.di.settings;

import com.koidev.androidgpsbroadcastv2.ui.settings.SettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = SettingsModule.class)
@SettingsScope
public interface SettingsComponent {
    void inject(SettingsFragment fragment);
}
