package com.koidev.androidgpsbroadcastv2.ui.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.koidev.androidgpsbroadcastv2.R;
import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.data.utils.RealmScheduler;
import com.koidev.androidgpsbroadcastv2.ui.login.ILogin;
import com.koidev.androidgpsbroadcastv2.ui.splash.SplashActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenter implements ILogin.Presenter {
    private RealmScheduler scheduler;
    private IAppInteractor interactor;
    private ILogin.View view;
    private RxPermissions rxPermision;
    //private StatusViewModel data;
    private Disposable disposableRequest, disposableMessage;

    public LoginPresenter(IAppInteractor iAppInteractor, RealmScheduler scheduler) {
        this.interactor = iAppInteractor;
        this.scheduler = scheduler;
    }

    @Override
    public void bindView(ILogin.View iLoginView) {
        this.view = iLoginView;
//        checkPermission();
    }

    @Override
    public void unbindView() {
        disposableMessage.dispose();
    }

    @Override
    public void checkFirstStartUp() {
        view.showProgress();
        disposableMessage = interactor.getMessageFromDB()
                .subscribeOn(scheduler.provideRealmScheduler())
                .unsubscribeOn(scheduler.provideRealmScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(message -> {
                    view.hideProgress();
                    if (!TextUtils.isEmpty(message)) {
                        showSplashScreen();
                    }
                });
    }

    @Override
    public void sendCodeEmei() {
        lockUI(true);
        String imei = getImeiFromView(), code = getCodeFromView();
        disposableRequest = interactor.sendRequestLogin(imei, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    //TODO: change checking
                    if (response.getStatus() != 0 && response.getDelay() != 0) {
                        interactor.savePersonalDataCache(
                                imei,
                                code,
                                response.getMessage(),
                                response.getCentral(),
                                response.getDelay(),
                                false);
                        lockUI(false);
                        showSplashScreen();
                    } else {
                        lockUI(false);
                        view.showDialogError(response.getMessage());
                    }

                }, throwable -> {
                    lockUI(false);
                    view.showDialogError("Network error!");
                });
    }

    private void lockUI(boolean lockInterface) {
        if (lockInterface) {
            view.showProgress();
        } else {
            view.hideProgress();
        }
    }

    private void showSplashScreen() {
        Intent intent = new Intent(view.getContext(), SplashActivity.class);
        view.getContext().startActivity(intent);
        view.getContext().finish();
    }

    @Override
    public void checkPermission() {
        rxPermision = new RxPermissions(view.getContext());
        rxPermision.request(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.SEND_SMS,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(granted -> {
                    if (!granted) {
                        view.showDialogError(view.getContext().getResources().getString(R.string.permission_error));
                        view.getContext().finish();
                    } else {
                        checkFirstStartUp();
                    }
                });
    }

    private String getImeiFromView() {
        TelephonyManager telephonyManager =
                (TelephonyManager) view.getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();
        if (TextUtils.isEmpty(imei)) {
            return "";
        } else {
            return imei;
        }
    }

    @Override
    public String getCodeFromView() {
        return view.getCode();
    }
}
