package com.koidev.androidgpsbroadcastv2.ui.contacts.helper;

public interface ItemTouchHelperViewHolder {

    void onItemSelected();

    void onItemClear();
}
