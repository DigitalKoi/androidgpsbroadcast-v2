package com.koidev.androidgpsbroadcastv2.ui.settings;

import android.content.Intent;

import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.ui.contacts.ContactsActivity;

public class SettingsPresenter implements ISettings.Presenter {

    private IAppInteractor iAppInteractor;
    private ISettings.View view;

    public SettingsPresenter(IAppInteractor iAppInteractor) {
        this.iAppInteractor = iAppInteractor;
    }

    @Override
    public void bindView(ISettings.View view) {
        this.view = view;
    }

    @Override
    public void showAddContactsActivity() {
        Intent intent = new Intent(view.getContext(), ContactsActivity.class);
        view.getContext().startActivity(intent);
    }

    @Override
    public void submitSettings() {
        iAppInteractor.saveMessageToDB(view.getMessageFromEdText());
        view.showToast("Settings saved!");
    }

    @Override
    public void getMessageFromCache() {
        iAppInteractor.getMessageFromDB().subscribe(message -> view.setMessage(message));
    }
}
