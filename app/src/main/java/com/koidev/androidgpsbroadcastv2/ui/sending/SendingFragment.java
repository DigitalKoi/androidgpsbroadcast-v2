package com.koidev.androidgpsbroadcastv2.ui.sending;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.koidev.androidgpsbroadcastv2.BroadcastApplication;
import com.koidev.androidgpsbroadcastv2.R;
import com.koidev.androidgpsbroadcastv2.di.sending.SendingModule;
import com.trncic.library.DottedProgressBar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;

public class SendingFragment extends Fragment implements ISending.View {

    private Unbinder unbider;
    private AlertDialog dialog;

    @Inject
    ISending.Presenter presenter;

    @BindView(R.id.constraintLayoutSending)
    ConstraintLayout layout;
    @BindView(R.id.stopCodeEd)
    EditText stopCodeEd;
    @BindView(R.id.progressSdBr)
    DottedProgressBar progressBar;

    public SendingFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BroadcastApplication.get(getContext())
                .applicationComponent()
                .plus(new SendingModule())
                .inject(this);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_sending, container, false);
        unbider = ButterKnife.bind(this, view);
        presenter.bindView(this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar.startProgress();
        dialog = new SpotsDialog(getContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.checkStatusDelay();
    }

    @Override
    public Activity getContext() {
        return getActivity();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }


    @OnClick(R.id.stopBt)
    public void sendStopCode(View view) {
        String stopCodeEdText = stopCodeEd.getText().toString();
        if (TextUtils.isEmpty(stopCodeEdText)) {
            showToast("Input stop code please");
        } else {
            presenter.sendStopCode(stopCodeEdText);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        progressBar.stopProgress();
        unbider.unbind();
        presenter.unbindingView();
    }
}
