package com.koidev.androidgpsbroadcastv2.ui.splash;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import com.koidev.androidgpsbroadcastv2.BroadcastApplication;
import com.koidev.androidgpsbroadcastv2.R;
import com.koidev.androidgpsbroadcastv2.di.splash.SplashModule;
import com.trncic.library.DottedProgressBar;

import javax.inject.Inject;

public class SplashFragment extends Fragment implements ISplash.View {

    @BindView(R.id.progressSpBr)
    DottedProgressBar progressBar;

    @Inject
    ISplash.Presenter iSplashPresenter;
    private Unbinder unbinder;

    public SplashFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BroadcastApplication.get(getContext())
                .applicationComponent()
                .plus(new SplashModule())
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_splash, container, false);
        unbinder = ButterKnife.bind(this, view);
        iSplashPresenter.bindView(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iSplashPresenter.showSendSMSActivity();
        progressBar.startProgress();
    }

    @Override
    public Activity getContext() {
        return getActivity();
    }

    @OnClick(R.id.closeImg)
    public void closeApp(View view) {
        iSplashPresenter.closeApp();
    }

    @OnClick(R.id.settingsImg)
    public void showSettings(View view) {
        iSplashPresenter.showSettings();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        progressBar.stopProgress();
        unbinder.unbind();
    }
}
