package com.koidev.androidgpsbroadcastv2.ui.sending;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.koidev.androidgpsbroadcastv2.R;

public class SendingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_sending);
    }
}
