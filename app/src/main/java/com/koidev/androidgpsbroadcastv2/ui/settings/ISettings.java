package com.koidev.androidgpsbroadcastv2.ui.settings;

import android.app.Activity;

public interface ISettings {

    interface View {

        String getMessageFromEdText();

        void setMessage(String message);

        void showToast(String message);

        Activity getContext();
    }

    interface Presenter {

        void bindView(ISettings.View view);

        void showAddContactsActivity();

        void submitSettings();

        void getMessageFromCache();
    }
}
