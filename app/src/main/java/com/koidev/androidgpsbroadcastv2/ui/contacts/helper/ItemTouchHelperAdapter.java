package com.koidev.androidgpsbroadcastv2.ui.contacts.helper;

public interface ItemTouchHelperAdapter {

    boolean onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
