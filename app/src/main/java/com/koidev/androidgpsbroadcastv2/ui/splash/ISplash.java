package com.koidev.androidgpsbroadcastv2.ui.splash;

import android.app.Activity;

public interface ISplash {

    interface View {
        Activity getContext();
    }

    interface Presenter {
        void bindView(ISplash.View iLoginView);

        void showSendSMSActivity();

        void closeApp();

        void showSettings();
    }
}
