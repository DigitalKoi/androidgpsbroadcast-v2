package com.koidev.androidgpsbroadcastv2.ui.contacts;

import android.app.Activity;

import com.koidev.androidgpsbroadcastv2.data.database.realm.ContactsDataDBRealm;

import java.util.List;

public interface IContacts {

    interface View {

        Activity getContext();

        void addContactsListToRecyclerView(List<ContactsDataDBRealm> list);

        void showProgressBar();

        void hideProgressBar();

        void showToast(String message);
    }

    interface Presenter {

        void bindView(IContacts.View view);

        void unbindView();

        void getContactsAllList();

        List<ContactsDataDBRealm> getCashedList();

        List<ContactsDataDBRealm> getCheckedList(List<ContactsDataDBRealm> filteredList);

        List<ContactsDataDBRealm> getSortedList(List<ContactsDataDBRealm> list);

        void setListFromAdapter(List<ContactsDataDBRealm> list);

        void saveCheckedContactsToDBAndClose();
    }
}
