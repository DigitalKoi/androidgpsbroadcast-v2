package com.koidev.androidgpsbroadcastv2.ui.contacts;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewAfterTextChangeEvent;
import com.koidev.androidgpsbroadcastv2.BroadcastApplication;
import com.koidev.androidgpsbroadcastv2.R;
import com.koidev.androidgpsbroadcastv2.data.database.realm.ContactsDataDBRealm;
import com.koidev.androidgpsbroadcastv2.di.contacts.ContactsModule;
import com.koidev.androidgpsbroadcastv2.ui.contacts.helper.EditItemTouchHelperCallback;
import com.koidev.androidgpsbroadcastv2.ui.contacts.helper.ItemAdapter;
import com.koidev.androidgpsbroadcastv2.ui.contacts.helper.OnStartDragListener;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class ContactsFragment extends Fragment implements IContacts.View, OnStartDragListener {

    private Unbinder unbinder;

    @Inject
    IContacts.Presenter iContactsPresenter;

    @BindView(R.id.searchEd)
    EditText searchEd;
    @BindView(R.id.contactsRV)
    RecyclerView contactsRV;
    @BindView(R.id.contactProgressBar)
    ProgressBar progressBar;
    ItemTouchHelper itemTouchHelper;

    private List<ContactsDataDBRealm> cachedList;
    private ItemAdapter itemAdapter;

    public ContactsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BroadcastApplication.get(getContext())
                .applicationComponent()
                .plus(new ContactsModule())
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_contacts, container, false);
        unbinder = ButterKnife.bind(this, view);
        iContactsPresenter.bindView(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contactsRV.requestFocus();
        iContactsPresenter.getContactsAllList();
        showFoundContacts();
    }

    public void showFoundContacts() {
        List<ContactsDataDBRealm> filteredList = new ArrayList<>();
        RxTextView.afterTextChangeEvents(searchEd)
                .debounce(500, TimeUnit.MILLISECONDS)
                .map(TextViewAfterTextChangeEvent::editable)
                .map(Object::toString)
                .doOnNext(findContact -> {
                    filteredList.clear();
                    cachedList = iContactsPresenter.getCashedList();
                    if (cachedList != null && !TextUtils.isEmpty(findContact)) {
                        for (ContactsDataDBRealm element : cachedList) {
                            if (element.getName().toLowerCase().contains(findContact.toLowerCase())
                                    || element.getNumber().contains(findContact)) {
                                filteredList.add(element);
                            }
                        }
                    } else if (itemAdapter != null) {
                        iContactsPresenter.setListFromAdapter(itemAdapter.getList());
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(findContact -> {
                    if (itemAdapter != null && filteredList.size() > 0 && !TextUtils.isEmpty(findContact)) {
                        itemAdapter.updateList(filteredList);
                    } else if (cachedList != null && itemAdapter != null) {
                        itemAdapter.updateList(cachedList);
                    }
                }, throwable -> Log.e("CoFragment", "showFoundContacts: " + throwable.toString()));
    }

    @Override
    public void addContactsListToRecyclerView(List<ContactsDataDBRealm> contactsList) {
        contactsRV.setLayoutManager(new LinearLayoutManager(getContext()));
        contactsRV.setHasFixedSize(true);
        itemAdapter = new ItemAdapter(getActivity(), contactsList, this);
        ItemTouchHelper.Callback callback = new EditItemTouchHelperCallback(itemAdapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(contactsRV);
        contactsRV.setAdapter(itemAdapter);
    }

    @OnClick(R.id.saveContactsBt)
    public void saveContactsToCache(View view) {
        iContactsPresenter.saveCheckedContactsToDBAndClose();
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        itemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        iContactsPresenter.unbindView();
    }

    @Override
    public Activity getContext() {
        return getActivity();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
