package com.koidev.androidgpsbroadcastv2.ui.sending;

import android.location.Location;

import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.data.database.realm.PersonalDataDBRealm;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SendingPresenter implements ISending.Presenter {

    private IAppInteractor interactor;
    private ISending.View view;

    private CompositeDisposable disposable = new CompositeDisposable();

    private String stopCode = "";


    public SendingPresenter(IAppInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void bindView(ISending.View view) {
        this.view = view;
        startServiceSending();

    }

    private void closeApp() {
        interactor.stopJobDispatcher();
        view.getContext().finish();
        view.getContext().moveTaskToBack(true);
        System.exit(0);
    }

    @Override
    public void unbindingView() {
        disposable.clear();
    }


    @Override
    public void sendStopCode(String stopCodeEdText) {
        view.showProgress();
        stopCode = stopCodeEdText;
        fetchData();
    }

    @Override
    public void checkStatusDelay() {
        //TODO: change listener without realm here
        interactor.addListenerDataBase()
                //Convert realm to PersonalDataDBRealm
                .map(realm -> {
                    PersonalDataDBRealm results = realm.where(PersonalDataDBRealm.class).findFirst();
                    assert results != null;
                    return realm.copyFromRealm(results);
                })
                //If delay 0 (service stop)
                .filter(data -> data.getDelay() == 0)
                .doOnNext(personalDataDBRealm -> {
                    //Set delay for next start App and close App
                    interactor.setDelayToDB(5);
                    closeApp();
                })
                .subscribe();

    }

    @Override
    public void startServiceSending() {
        interactor.checkLocationSettingsGPS().subscribe();
        interactor.startJobDispatcher();
    }

    private void fetchData() {
        Single.zip(
                interactor.getLocation().firstOrError(),
                interactor.checkPersonalDataCache().firstOrError(),
                interactor.getBatteryLevel().firstOrError(), this::sendRequest)
                .subscribe();
    }

    private Disposable sendRequest(
            Location location,
            PersonalDataDBRealm personal,
            String battery) {
        return interactor.sendRequestLocationData(
                                personal.getImei(),
                                stopCode,
                                convertDate(location.getTime()),
                                 location.getLongitude() + "|" + location.getLatitude(),
                                String.valueOf(location.getSpeed()),
                                battery)
                .doOnSubscribe(disposable::add)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    //TODO: change checking
                    if (response.getStatus() == 0 && response.getDelay() == 0) {
                        view.hideProgress();
                        closeApp();
                    } else {
                        view.hideProgress();
                        view.showToast("Error code!");
                    }
                });
    }

    private String convertDate(Long longDate) {
        Date date = new Date(longDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(date) + "T" + timeFormat.format(date);
    }
}
