package com.koidev.androidgpsbroadcastv2.ui.login;

import android.app.Activity;

public interface ILogin {

    interface View {
        void showProgress();

        void hideProgress();

        Activity getContext();

        String getCode();

        void showDialogError(String message);

        void showToast(String message);
    }

    interface Presenter {
        void bindView(ILogin.View iLoginView);

        void unbindView();

        void checkPermission();

        String getCodeFromView();

        void sendCodeEmei();

        void checkFirstStartUp();
    }
}
