package com.koidev.androidgpsbroadcastv2.ui.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.koidev.androidgpsbroadcastv2.BroadcastApplication;
import com.koidev.androidgpsbroadcastv2.R;
import com.koidev.androidgpsbroadcastv2.di.login.LoginModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;

public class LoginFragment extends Fragment implements ILogin.View {

    private Unbinder unbinder;
    private AlertDialog dialog;

    @Inject
    ILogin.Presenter iLoginPresenter;

    @BindView(R.id.authcodeEd)
    EditText codeEd;

    public LoginFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BroadcastApplication.get(getContext())
                .applicationComponent()
                .plus(new LoginModule())
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        iLoginPresenter.bindView(this);

        dialog = new SpotsDialog(getContext());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iLoginPresenter.checkPermission();
    }

    @OnClick(R.id.confirmBt)
    public void sendCodeEmei(View view) {
        iLoginPresenter.sendCodeEmei();
    }

    @Override
    public void showProgress() {
//    progressBar.setVisibility(progressBar.VISIBLE);
        dialog.show();
    }

    @Override
    public void hideProgress() {
//    progressBar.setVisibility(View.GONE);
        dialog.dismiss();
    }

    @Override
    public String getCode() {
        return String.valueOf(codeEd.getText());
    }

    @Override
    public void showDialogError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Activity getContext() {
        return getActivity();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
