package com.koidev.androidgpsbroadcastv2.ui.sending;

import android.app.Activity;

public interface ISending {

    interface View {
        Activity getContext();

        void showToast(String message);

        void showProgress();

        void hideProgress();

    }

    interface Presenter {
        void bindView(ISending.View view);

        void unbindingView();

        void sendStopCode(String stopCodeEdText);

        void checkStatusDelay();

        void startServiceSending();
    }

}
