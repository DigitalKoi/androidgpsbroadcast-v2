package com.koidev.androidgpsbroadcastv2.ui.contacts.helper;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koidev.androidgpsbroadcastv2.R;
import com.koidev.androidgpsbroadcastv2.data.database.realm.ContactsDataDBRealm;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.VHItem>
        implements ItemTouchHelperAdapter {

    private static final int TYPE_ITEM = 0;
    private List<ContactsDataDBRealm> contactList;
    private List<ContactsDataDBRealm> checkedList;

    public ItemAdapter(Context context, List<ContactsDataDBRealm> list,
                       OnStartDragListener dragListener) {
        this.contactList = list;
        checkedList = new ArrayList<>();
    }

    @Override
    public VHItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item, parent, false);
        return new VHItem(view);
    }

    @Override
    public void onBindViewHolder(final ItemAdapter.VHItem holder, int position) {
        int adapterPosition = holder.getAdapterPosition();
        ContactsDataDBRealm contact = contactList.get(adapterPosition);

        String contactName = contact.getName();

        holder.contactName.setText(contactName);
        holder.contactNumber.setText(contact.getNumber());

        //TODO: Show images for contacts
        //viewHolder.contactsImage.setImageURI(contactList.get(i).getImageUri());

        holder.checkImage.setImageResource(
                contact.isChecked() ? R.drawable.phone_yellow : R.drawable.phone_green);

        holder.checkImage.setOnClickListener(view -> {
            if (contact.isChecked()) {
                //remove contact from checked list
                if (checkedList.indexOf(contact) > -1) {
                    checkedList.remove(contact);
                }
                //update contact
                contact.setChecked(false);
                contactList.set(contactList.indexOf(contact), contact);
                //move item to the end of the list
                onItemMove(contactList.indexOf(contact), contactList.size() - 1);
            } else if (!contact.isChecked()) {
                //update contact
                contact.setChecked(true);
                contactList.set(contactList.indexOf(contact), contact);
                //move item to the beginning of the list
                onItemMove(contactList.indexOf(contact), checkedList.size() /*0*/);
                //add contact to checked list
                checkedList.add(contact);
            }
        });
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        ContactsDataDBRealm contact = contactList.get(fromPosition);
        contactList.remove(fromPosition);
        contactList.add(toPosition, contact);
        notifyItemMoved(fromPosition, toPosition);
        notifyItemChanged(toPosition);
        notifyItemChanged(fromPosition);
        return true;
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onItemDismiss(int position) {
        contactList.remove(position);
        notifyItemRemoved(position);
    }

    public void updateList(List<ContactsDataDBRealm> list) {
        contactList = list;
        notifyDataSetChanged();
    }

    public List<ContactsDataDBRealm> getList() {
        return contactList;
    }

    public static class VHItem extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {

        private TextView contactName;
        private TextView contactNumber;
        private ImageView contactsImage;
        private ImageView checkImage;

        public VHItem(View itemView) {
            super(itemView);
            contactName = itemView.findViewById(R.id.contactNameTv);
            contactNumber = itemView.findViewById(R.id.contactNumberTv);
            contactsImage = itemView.findViewById(R.id.contactImageImg);
            checkImage = itemView.findViewById(R.id.contactCheckImg);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.BLUE);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }
}