package com.koidev.androidgpsbroadcastv2.ui.splash;

import android.content.Intent;

import com.koidev.androidgpsbroadcastv2.data.utils.Constants;
import com.koidev.androidgpsbroadcastv2.ui.sending.SendingActivity;
import com.koidev.androidgpsbroadcastv2.ui.settings.SettingsActivity;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SplashPresenter implements ISplash.Presenter {

  private ISplash.View iSplashView;
  private Disposable disposableStartSendingSMS;

  @Override public void bindView(ISplash.View iLoginView) {
    this.iSplashView = iLoginView;
  }

  @Override public void showSendSMSActivity() {
    disposableStartSendingSMS = Single.just(true)
        .delay(Constants.SPLASH_DELAY, TimeUnit.SECONDS)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(o -> {
          Intent intent = new Intent(iSplashView.getContext(), SendingActivity.class);
          iSplashView.getContext().startActivity(intent);
          iSplashView.getContext().finish();
  });
  }

  @Override public void showSettings() {
    disposableStartSendingSMS.dispose();
    Intent intent = new Intent(iSplashView.getContext(), SettingsActivity.class);
    iSplashView.getContext().startActivity(intent);
  }

  @Override public void closeApp() {
    disposableStartSendingSMS.dispose();
    iSplashView.getContext().moveTaskToBack(true);
    iSplashView.getContext().finish();
    System.exit(0);
  }
}
