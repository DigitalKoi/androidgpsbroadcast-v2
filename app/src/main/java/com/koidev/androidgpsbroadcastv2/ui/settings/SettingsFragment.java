package com.koidev.androidgpsbroadcastv2.ui.settings;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.koidev.androidgpsbroadcastv2.BroadcastApplication;
import com.koidev.androidgpsbroadcastv2.R;
import com.koidev.androidgpsbroadcastv2.di.settings.SettingsModule;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;

public class SettingsFragment extends Fragment implements ISettings.View {

    @Inject
    ISettings.Presenter iSettingsPresenter;

    private Unbinder unbinder;
    @BindView(R.id.messageEd)
    EditText messageEd;
    @BindView(R.id.countMessageSymbolsTv)
    TextView countMessageSymbolsTv;

    public SettingsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BroadcastApplication.get(getContext())
                .applicationComponent()
                .plus(new SettingsModule())
                .inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_settings, container, false);
        unbinder = ButterKnife.bind(this, view);
        iSettingsPresenter.bindView(this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iSettingsPresenter.getMessageFromCache();
        showCurrentCountSymbols();
    }

    private void showCurrentCountSymbols() {
        RxTextView.textChanges(messageEd)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(message -> countMessageSymbolsTv.setText(message.length() + "/100"));
    }

    @OnClick(R.id.addcontactBt)
    public void addContact(View view) {
        iSettingsPresenter.showAddContactsActivity();
    }

    @OnClick(R.id.submitBt)
    public void submitSettings(View view) {
        iSettingsPresenter.submitSettings();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public String getMessageFromEdText() {
        return String.valueOf(messageEd.getText());
    }

    @Override
    public void setMessage(String message) {
        messageEd.setText(message);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Activity getContext() {
        return getActivity();
    }
}
