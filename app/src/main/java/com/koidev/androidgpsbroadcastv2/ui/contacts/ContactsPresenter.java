package com.koidev.androidgpsbroadcastv2.ui.contacts;

import android.content.Intent;
import android.util.Log;

import com.koidev.androidgpsbroadcastv2.business.IAppInteractor;
import com.koidev.androidgpsbroadcastv2.data.database.realm.ContactsDataDBRealm;
import com.koidev.androidgpsbroadcastv2.ui.sending.SendingActivity;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;

public class ContactsPresenter implements IContacts.Presenter {
    private IAppInteractor interactor;
    private IContacts.View view;
    private List<ContactsDataDBRealm> cachedList = new ArrayList<>();
    private List<ContactsDataDBRealm> contactsFromAdapter;
    private Disposable subscribeContactList;

    public ContactsPresenter(IAppInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void bindView(IContacts.View view) {
        this.view = view;
    }

    @Override
    public void unbindView() {
        subscribeContactList.dispose();
    }

    @Override
    public void getContactsAllList() {
        subscribeContactList = interactor.getAllContacts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(element -> cachedList.add(element))
                .doOnSubscribe(disposable -> {
                    view.showProgressBar();
                    cachedList.clear();
                })
                .doOnComplete(() -> {
                    view.hideProgressBar();
                    view.addContactsListToRecyclerView(getCashedList());
                })
                .subscribe();
    }

    @Override
    public List<ContactsDataDBRealm> getCashedList() {
        int lastIndexIsChecked = 0;
        for (int i = 0; i < cachedList.size(); i++) {
            ContactsDataDBRealm tempElement = cachedList.get(i);
            if (tempElement.isChecked()) {
                Log.i("ContPresenter", "getCashedList: " + tempElement.getName());
                cachedList.remove(i);
                cachedList.add(lastIndexIsChecked, tempElement);
                lastIndexIsChecked++;
            }
        }
        return cachedList;
    }

    @Override
    public List<ContactsDataDBRealm> getCheckedList(List<ContactsDataDBRealm> filteredList) {
        List<ContactsDataDBRealm> tempList = new ArrayList<>();
        for (ContactsDataDBRealm contact : filteredList) {
            if (contact.isChecked()) {
                tempList.add(contact);
            }
        }
        return tempList;
    }

    @Override
    public List<ContactsDataDBRealm> getSortedList(List<ContactsDataDBRealm> unsortedList) {
        int lastIndexIsChecked = 0;
        for (int i = 0; i < unsortedList.size(); i++) {
            ContactsDataDBRealm tempElement = unsortedList.get(i);
            if (tempElement.isChecked()) {
                unsortedList.remove(i);
                unsortedList.add(lastIndexIsChecked, tempElement);
                lastIndexIsChecked++;
            }
        }
        return unsortedList;
    }

    @Override
    public void setListFromAdapter(List<ContactsDataDBRealm> list) {
        Single.fromCallable(() -> list).subscribeOn(Schedulers.io()).subscribe(list1 -> {
            if (cachedList.size() > 0 && list1.size() > 0) {
                contactsFromAdapter = list1;
                for (int i = 0; i < cachedList.size(); i++) {
                    for (int j = 0; j < contactsFromAdapter.size(); j++) {
                        ContactsDataDBRealm fromPresenter = cachedList.get(i);
                        ContactsDataDBRealm fromAdapter = contactsFromAdapter.get(j);
                        if (fromPresenter.getId() == fromAdapter.getId()) {
                            if (fromPresenter.isChecked() != fromAdapter.isChecked()) {
                                cachedList.get(i).setChecked(fromAdapter.isChecked());
                            }
                        }
                    }
                    cachedList = getSortedList(cachedList);
                }
            }
        });
    }

    @Override
    public void saveCheckedContactsToDBAndClose() {
        interactor.saveContactsCache(getCheckedList(cachedList));
        unbindView();
        Intent intent = new Intent(view.getContext(), SendingActivity.class);
        view.getContext().startActivity(intent);
        view.getContext().finish();
    }
}

